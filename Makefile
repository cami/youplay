install:
	rm -fr /usr/bin/youplay
	cp youplay.py /usr/bin/youplay
	chmod +x /usr/bin/youplay
	cp youplay.desktop /usr/share/applications/
	mkdir /usr/local/share/youplay
	cp youplay.svg /usr/local/share/youplay/
init:
	pip3 install --upgrade youtube-dl
	pip3 install --upgrade python-mpv
