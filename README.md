# Welcome to YouPlay

Do you love music, but you hate restrictions?

With YouPlay you can search for whatever music song in your mind. It will list a number of matching songs. You can listen to it and it will be downloaded as an mp3 file.

Youplay is a single Python file with some dependencies.

```
![YouPlay Screenshot](https://codeberg.org/ralfhersel/youplay/src/branch/main/youplay.png)
```

## Installation

Currently the is no Flatpak, Snap, AppImage or other package available. You can install it to your system with a simple Makefile or use the manual install method described below.

##### Makefile

> Before you start the Makefile, ensure in your system's package manager if these packages are installed: `mpv`, `libmpv1`, `ffmpeg`,  `python3-pip`

```bash
git clone https://codeberg.org/ralfhersel/youplay.git
cd youplay

make init
sudo make
```

Launch the GUI in your Application Menu and the CLI with the `youplay`-Command.

##### Manual Installation

The installation method is still the prefered one, until an installable package is available.

```bash
git clone https://codeberg.org/ralfhersel/youplay.git
cd youplay
```

After the files are downloaded, install the dependencies according to the instructions given in the next chapter 'Dependencies'. Make the `youplay.py` executable in your file manager. Change the pathes (Exec and Icon) in `youplay.desktop` to your pathes. Copy `youplay.desktop` to `/home/user/.local/share/applications`.

## Dependencies

YouPlay has some dependencies, which are:

* python3 (required to run a python script)
  should already be part of your distribution

* pip3 (required to install the python modules)
  install with: `python3-pip` in your Distro's Package Manager

* mpv (required for the CLI player)
  install with: `mpv` in your Distro's Package Manager

* mpvlib (required for the CLI player)
  install with: `libmpv1` in your Distro's Package Manager

* ffmpeg (required to calculate the duration of mp3 files)
  install with: `ffmpeg` in your Distro's Package Manager  

* python-mpv (required for the GUI player)
  install with: `pip3 install --upgrade python-mpv`

* youtube-dl (required to search and download music from Youtube)
  install with: `pip3 install --upgrade youtube-dl`

Please install these dependencies before you start YouPlay.

## Operation Instructions

YouPlay supports two modes: CLI (command line mode) and GUI (graphical mode).

You can start YouPlay in CLI mode like this:

* ./youplay.py

* ./youplay.py --help

* ./youplay.py songtitle

All other options will be shown interactively. In this mode, the audio player MPV will be shown in CLI-mode as well.

You can also start YouPlay in GUI mode:

* ./youplay.py --gui
* requires `youplay.desktop` with adapted pathes and copied to `/home/user/.local/share/applications/`

When the GUI shows up, you can enter a song title that you want to listen to. The App will present a list of 10 matching songs from Youtube. Select one or doubleclick to start downloading and playing it. In GUI mode, YouPlay will start MPV in internal mode. All songs that you are listened to, are stored as mp3 files in the 'music' subfolder.

A song, that was already downloaded, will not be downloaded again, as long as the file exists in the 'music' subfolder. You can show and play all downloaded songs with the SONG button. Just enter another search term to return to the search list.

## Release Notes

###### Version 0.26 - 2021.01.08

* Heavy refactoring on the CLI control loop, run ''./youplay.py --help' for more infos

* CLI mode now shows already downloaded songs, run: './'youplay.py --songs'

* Further list alignment for song and search list, both in CLI- and GUI-mode.

* Installation scripts and packages are 'work in progress' and not yet ready.

###### Version 0.25 - 2021.01.04

* Code clean-up

* Mutagen dependency removed (song duration is now fetched with 'ffmpeg')

###### Version 0.24 - 2021.01.03

* GUI song number centered

* Only one button to PLAY or STOP a song

* Doubleclick on a song while another one is playing, will stop the playing song and play the doubleclicked song

* Progress bar works when a second song is searched

* The SONG-button will show and let you play all downloaded songs from the 'music' folder

###### Version 0.23 - 2020.12.23

* GUI mode shows playing time

* Columns and song titles are correctly adjusted to fit into window

###### Version 0.22 - 2020.12.21

* GUI mode will utilize the internal python-mpv player instead of the external mpv player

* PLAY-button will be disabled if you doubleclick song title or click the PLAY-button

###### Version 0.21 - 2020.12.19 (my birthday version)

* ffmpeg dependency removed, because youtube-dl will download mp3 directly

* Downloaded songs will be stored in the 'music' subfolder

* Already downloaded songs will not be downloaded again, but played immediately

* Option to save a song removed from CLI and GUI, because it is obsolete

* PLAY button disabled when song is playing (only when started from PLAY button, not from doubleclick)

###### Version 0.20 - 2020.12.18

* Info about song duration added to CLI and GUI

* Endless streams are omitted (by duration = 0)

* Number of songs reduced to 10 (to avoid lenghty loading time)

###### Version 0.19 - 2020.12.16

* Progress bar while searching, downloading and saving songs
* Progress bar for CLI and GUI mode

###### Version 0.18 - 2020.12.15

* youplay.desktop-file doesn't require bash-script, but call youplay.py directly

* CLI-mode utilises MPV in CLI-mode

* GUI-mode starts MPV in GUI-mode

* Doubleclick in song list, starts download and playback of selected song

###### Versions before 0.18 - not documented

* sorry

## License

Author: Ralf Hersel

License: GPL3

Repository: https://codeberg.org/ralfhersel/youplay.git

## Contact

https://matrix.to/#/@ralfhersel:feneas.org
