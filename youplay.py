#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Name:           youplay.py
Description:    search, play and save audio files from Youtube
Usage:          start it with: './youplay.py' or './youplay.py song' or './youplay.py gui'
Author:         Ralf Hersel - ralf.hersel@gmx.net
Licence:        GPL3
More infos:     see constants and readme.md
'''

# === Todo =====================================================================

'''
- create installer
'''

# === Dependencies =============================================================

'''
python3    - default on most GNU/Linux-Distros                                  # because it is Python :)
youtube-dl - pip3 install --upgrade youtube-dl                                  # search, download from Youtube
mpv        - sudo apt install mpv                                               # stand-alone player
python-mpv - pip3 install --upgrade python-mpv                                  # python bindings for the integrated MPV player
ffmpeg     - sudo apt install ffmpeg                                            # required to calculate mp3 duration
'''

# === Libraries ================================================================

import subprocess
import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib
import time
import sys
import mpv
import glob                                                                     # for directory patterned file list (mp3)


# === Constants ================================================================

PROG_NAME        = 'YouPlay'
VERSION          = '0.26'
LAST_UPDATE      = '2021-01-08'
LICENCE          = 'GPL3'
AUTHOR           = 'Ralf Hersel'
WIN_WIDTH        = 750
WIN_HEIGHT       = 373
LINE             = '-' * 80
NUMBER_OF_SONGS  = 10
MAX_TITLE_LENGHT = 65
MUSIC_FOLDER     = 'music/'

HELP = '''YouPlay Help:
================================================================================
-h    --help       show command line parameter
-v    --version    show version information
-a    --about      show general infos about the application
-g    --gui        start YouPlay in GUI-mode (otherwise CLI-mode)
-s    --songs      list already downloaded songs
[a song title]     song title, that you want to search for'''

WELCOME = '''Welcome to {}
================================================================================
'''.format(PROG_NAME)

CONTROLS = '''Player controls:
    q       quit
    space   pause/continue
    right   fast forward
    left    fast backward'''

THANKS = '''================================================================================
Thank you for using {}!'''.format(PROG_NAME)

ABOUT = '''Reclaim Freedom
Suck it from Youtube
Fight against digital handcuffs

Version    : {}
Update     : {}
Licence    : {}
Author     : {}
Contact    : @ralfhersel:feneas.org

Stay free, play music, donate artists'''.format(VERSION, LAST_UPDATE, LICENCE, AUTHOR)


# === Features =================================================================

def get_songlist(song):                                                         # get songlist from Youtube
    filename = 'audio.list'
    print('Retrieving song list from Youtube:', song, '\nPlease wait ', end='')
    command = 'youtube-dl --get-title --get-duration "ytsearch{}:{}" > {}'.format(str(NUMBER_OF_SONGS), song, filename)
    p = subprocess.Popen(command, shell=True)
    wait_until_process_finished(p)
    song_dict = {}
    f = open(filename)
    count = 1                                                                   # file line counter
    number = 1                                                                  # list line counter
    for line in f:
        striped_line = line.strip()
        if count % 2 == 0:                                                      # even = duration
            duration = striped_line
            if duration != '0':                                                 # no stream
                song_dict[str(number)] = [title, duration]
                number += 1
        else:                                                                   # not even = titel
            title = striped_line[:MAX_TITLE_LENGHT].ljust(MAX_TITLE_LENGHT, ' ') # fit title lenght
        count += 1
    f.close()
    return song_dict


def show_list(song_dict):                                                       # show the list of songs
    print(LINE)
    for key, item in song_dict.items():
        number = str(key)
        titel = item[0]
        duration = item[1]
        print(number.ljust(3), titel, '-', duration.rjust(8))
    print(LINE)
    keep_asking = True
    while keep_asking:
        answer = input('\nSong number or RETURN for first song or (q)uit: ')
        if answer == 'q':                                                       # quit
            keep_asking = False
        elif len(answer) == 0:                                                  # return
            answer = '1'                                                        # play first song
            keep_asking = False
        elif not answer.isdigit():                                              # wrong letter
            print('WARNING: wrong entry, Try again.')
        elif int(answer) in range(1, NUMBER_OF_SONGS + 1):                      # check number
            keep_asking = False
        else:
            print('WARNING: wrong number. Try again.')
    return answer        


def get_song(song):                                                             # Get song from Youtube and download it
    if not os.path.exists('music'):                                             # create sub-dir 'music' if not exists
        os.mkdir('music')
    if song[-3:] != 'mp3':                                                      # check for (none) existing mp3 extention
        song_list = song.split()
        audio_filename = '_'.join(song_list) + '.mp3'                           # construct filename
        audio_filename = sanitize_filename(audio_filename)
        audio_path = MUSIC_FOLDER + audio_filename
    else:
        audio_path = MUSIC_FOLDER + song
    if not os.path.exists(audio_path):
        print('\nDownloading song from Youtube:', song,
            '\nPlease wait until the song is downloaded ', end='')
        p = subprocess.Popen(['youtube-dl', '-q', '-f bestaudio', '--max-downloads',
         '1', '--yes-playlist', '--default-search', 'ytsearch', song,
         '--audio-format', 'mp3', '-o', audio_path])
        wait_until_process_finished(p)
    return audio_path
    

def play_song(audio_path):                                                      # play song from CLI
    print(CONTROLS)
    p = subprocess.Popen(['mpv', audio_path])                                   # play the song with MPV
    p.wait()
    p.terminate()


def show_downloaded_songs():                                                    # show list of downloaded songs
    files=glob.glob(MUSIC_FOLDER + '*.mp3')
    try: filename = files[0]
    except IndexError:
        self.show_status_text('No songs in music folder')
        answer = 'q'
        return answer
    number = 0
    song_dict = {}
    for filepath in files:
        title = os.path.basename(filepath)                                      # get filename from path
        title = title[:MAX_TITLE_LENGHT].ljust(MAX_TITLE_LENGHT, ' ')           # cut and fit title length
        number += 1
        duration = get_mp3_duration(filepath)
        song_dict[str(number)] = [title, duration]
    keep_asking = True
    while keep_asking:
        print('Downloaded songs')
        print(LINE)
        for key, item in song_dict.items():
            number = str(key)
            title = item[0]
            duration = item[1]
            print(number.ljust(3), title, '-', duration)
        print(LINE)
        answer = input('\nSong number or RETURN for first song or (q)uit: ')
        if answer == 'q':                                                       # quit
            return answer
        elif len(answer) == 0:                                                  # return
            answer = '1'                                                        # play first song
            song = song_dict[answer][0]                                         # get song name
            audio_path = get_song(song)                                         # download song
            play_song(audio_path)                                               # play song
        elif not answer.isdigit():                                              # wrong letter
            print('WARNING: wrong entry, Ty again.')
        elif int(answer) in range(1, len(song_dict)+1):                         # check number
            song = song_dict[answer][0]                                         # get song name
            audio_path = get_song(song)                                         # download song
            play_song(audio_path)                                               # play song
        else:
            print('WARNING: wrong number. Try again.')
    return answer


# === Misc =====================================================================

def sanitize_filename(filename):                                                # create proper filename
    sanitized_filename = ''.join( x for x in filename if (x.isalnum() or x in "._- "))
    return sanitized_filename


def wait_until_process_finished(p):                                             # do things until process finished
    while p.poll() is None:                                                     # subprosses still running?
        print('.', end='')                                                      # CLI progress bar
        sys.stdout.flush()                                                      # update display
        now = time.time()                                                       # next 3 lines replaces 'sleep(1)'
        while time.time() - now < 1:
            Gtk.main_iteration_do(False)
    print()                                                                     # add break to finish breakless print


def get_mp3_duration(filepath):                                                 # calculate duration of mp3 file
    result = subprocess.run(["ffprobe", "-v", "error", "-show_entries",
                             "format=duration", "-of",
                             "default=noprint_wrappers=1:nokey=1", filepath],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    duration = float(result.stdout)
    sec_time_object = time.gmtime(duration)                                     # convert to time object
    hms = time.strftime("%H:%M:%S",sec_time_object)                             # convert seconds to hour:minute:second
    return hms


#~ === GUI =====================================================================

class Gui:

    def __init__(self):
        # === Variables ========================================================
        
        self.player = mpv.MPV(ytdl=True, input_default_bindings=True,
            input_vo_keyboard=True)                                             # initialize mpv internal player


        @self.player.property_observer('time-pos')
        def time_observer(_name, value):
            if value is not None:                                               # avoid Type Error when started
                sec = int(value)                                                # get seconds from flaot
                sec_time_object = time.gmtime(sec)                              # convert to time object
                hms = time.strftime("%H:%M:%S",sec_time_object)                 # convert seconds to hour:minute:second
                self.show_status_text(self.status_text + ' Time : ' + hms)      # update status bar
        
        # === Widgets ==========================================================

        # Window
            # Box (vertical)
                # Entry
                # ScrolledWindow
                    # TreeView
                        # TreeViewColumn
                # Box (horizontal)
                    # Buttons
                # StatusBar
                # ProgressBar
        
        self.window = Gtk.Window()                                              # Window
        self.window.set_title(PROG_NAME + ' it from Youtube')
        self.window.set_default_icon_from_file('youplay.svg')
        self.window.set_default_size(WIN_WIDTH, WIN_HEIGHT)                     # width, height
        self.window.connect('delete_event', self.on_window_delete)              # Window red-cross clicked
        
        self.main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)           # Vertical Box
        self.window.add(self.main_box)

        self.entry = Gtk.Entry()                                                # Entry
        self.entry.connect('activate', self.on_entry_return)                    # Return pressed
        self.entry.set_text('Enter song name')
        self.main_box.pack_start(self.entry, False, False, 0)
        
        self.liststore = Gtk.ListStore(str, str, str)                           # Treeview and Liststore
        self.treeview = Gtk.TreeView(model=self.liststore)
        self.treeview.connect('button-press-event', self.on_treeview_clicked)	# Treeview item clicked
        for i, column_title in enumerate(['Nr', 'Title', 'Duration']):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(column_title, renderer, text=i)
            if i == 0:                                                          # Nr
                column.set_alignment(0.5)										# center Nr heading
                renderer.set_property('xalign', 0.5)                            # center Nr content
            if i == 2:                                                          # Duration
                column.set_alignment(1)										    # right align column heading
                renderer.set_property('xalign', 1)                              # right align column content
            self.treeview.append_column(column)

        self.scrollable_treelist = Gtk.ScrolledWindow()
        self.scrollable_treelist.add(self.treeview)
        self.main_box.pack_start(self.scrollable_treelist, True, True, 0)
        
        self.button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)       # Horizontal Button Box
        
        self.button_play = Gtk.Button(label='Play')
        self.button_play.connect('clicked', self.on_button_play_clicked)
        self.button_box.pack_start(self.button_play, True, True, 0)

        self.button_songs = Gtk.Button(label='Songs')
        self.button_songs.connect('clicked', self.on_button_songs_clicked)
        self.button_box.pack_start(self.button_songs, True, True, 0)
        
        self.button_about = Gtk.Button(label='About')
        self.button_about.connect('clicked', self.on_button_about_clicked)
        self.button_box.pack_start(self.button_about, True, True, 0)
        
        self.button_quit = Gtk.Button(label='Quit')
        self.button_quit.connect('clicked', self.on_button_quit_clicked)
        self.button_box.pack_start(self.button_quit, True, True, 0)
        
        self.main_box.add(self.button_box)
        
        self.status_bar = Gtk.Statusbar()
        self.main_box.add(self.status_bar)

        self.progressbar = Gtk.ProgressBar()
        self.progressbar.set_pulse_step(0.2)
        self.timeout_id = GLib.timeout_add(200, self.on_timeout, None)          # progress bar speed
        self.main_box.add(self.progressbar)
        
        self.window.show_all()                                                  # show GUI
        self.progressbar.hide()


# === Event Handler ============================================================

    def on_window_delete(self, widget, event, data=None):                       # close the window by red cross
        self.player.stop()
        del self.player
        Gtk.main_quit()


    def on_entry_return(self, widget):                                          # Return pressed in entry field
        label = self.button_play.get_label()                                    # Enable new search when song is playing
        if label == 'Stop':
            self.player.stop()
            self.show_status_text('Stopped playing')
            
        self.progressbar.hide()
        self.button_play.set_label('Play')
        self.progressbar.show()
        self.show_status_text('Retrieving song list from Youtube ... please wait')
        song = widget.get_text()
        song_dict = get_songlist(song)
        song_list = song_dict.items()                                           # convert dict to list
        if len(song_list) == 0:                                                 # nothing found
            self.show_status_text('Nothing found on Youtube. Try another search.')
        else:
            self.liststore.clear()
            for key, item in song_dict.items():
                number = key
                title = item[0]
                duration = item[1]
                self.liststore.append([number, title, duration])
            self.show_status_text('Select a song and click Play')
        self.progressbar.hide()


    def on_treeview_clicked(self, widget, event):                               # click on treeview item
        if event.type == Gdk.EventType._2BUTTON_PRESS:                          # but only doubleclick
            self.player.stop()                                                  # stop already playing song
            selected = self.get_tree_selection(self.treeview)
            self.do_play(selected)


    def on_button_play_clicked(self, widget):                                   # PLAY/STOP clicked
        label = widget.get_label()
        if label == 'Play':
            selected = self.get_tree_selection(self.treeview)
            self.do_play(selected)
        if label == 'Stop':
            widget.set_label('Play')
            self.player.stop()
            self.show_status_text('Stopped playing')


    def on_button_songs_clicked(self, widget):                                  # show list of downloaded songs
        files=glob.glob(MUSIC_FOLDER + '*.mp3')
        try: filename = files[0]
        except IndexError:
            self.show_status_text('No songs in music folder')
            return

        self.liststore.clear()
        number = 0
        for filepath in files:
            title = os.path.basename(filepath)                                  # get filename from path
            title = title[:MAX_TITLE_LENGHT+10].ljust(MAX_TITLE_LENGHT, ' ')    # cut and fit title length
            number += 1
            duration = get_mp3_duration(filepath)
            self.liststore.append([str(number), title, duration])
        self.show_status_text('Select a song and click Play')
        

    def on_button_about_clicked(self, widget):                                  # ABOUT clicked
        self.show_info_dialog(PROG_NAME, ABOUT)


    def on_button_quit_clicked(self, widget):                                   # QUIT clicked
        self.player.stop()
        del self.player
        Gtk.main_quit()


    def on_timeout(self, user_data):                                            # update value on progress bar
        self.progressbar.pulse()
        return True


# === Methods ==================================================================

    def do_play(self, selected):                                                # play song from GUI
        if selected == None:
            self.show_status_text('Nothing selected')
        else:
            self.progressbar.show()
            self.show_status_text('Downloading song from Youtube ... please wait')
            song = selected[1]
            audio_path = get_song(song)
            self.progressbar.hide()
            self.status_text = 'Playing: ' + selected[1]                        # status bar will be updated by mpv time_observer
            self.button_play.set_label('Stop')
            self.player.play(audio_path)


    def get_tree_selection(self, tree):                                         # get selected tree item
        selection = tree.get_selection()
        model, treeiter = selection.get_selected()
        if treeiter != None:
            result = model[treeiter]
        else:
            result = None
        return result
        
    
    def show_status_text(self, text):                                           # show status text
        self.status_bar.push(0, text)
        while Gtk.events_pending():
            Gtk.main_iteration_do(True)                                         # wait until statusbar is refreshed


    def show_info_dialog(self, title, text):                                    # Info dialog
        dialog = Gtk.MessageDialog(message_type=Gtk.MessageType.INFO,
            buttons=Gtk.ButtonsType.OK, text=text)
        dialog.set_title(title)
        dialog.run()
        dialog.destroy()


# === Main =====================================================================

def main(args):
    print(WELCOME)
    answer = ' '.join(args[1:])                                                 # get cli input and concat args
    answer = answer.lower()                                                     # convert to lowercase
    song_dict = {}
    question = 'Enter (q)uit, (l)ist, (s)ongs or song title: '
    finish = False
    while finish == False:                                                      # command loop
        if answer == '-h' or answer == '--help':                                # show help
            print(HELP)
            finish = True
        elif answer == '-v' or answer == '--version':                           # show version
            print(PROG_NAME, '- Version:', VERSION, '-',LAST_UPDATE, '-',LICENCE, '-',AUTHOR, '\n')
            finish = True
        elif answer == '-a' or answer == '--about':                             # show about infos
            print(ABOUT)
            finish = True
        elif answer == '-g' or answer == '--gui':                               # show GUI
            gui = Gui()
            Gtk.main()
            finish = True                                                       # leave the loop
        elif answer == '':
            answer = input(question)                                            # no song from command line
            if answer == '':
                print('Error: wrong entry')
                finish = True
            elif answer == 'q':
                finish = True
            elif answer == 's':
                show_downloaded_songs()
                answer = input(question)
            elif answer == 'l':
                print('Warning: there are no search results')
                answer = input(question)
            elif answer[0] != '-' and answer[:2] != '--':                       # song title entered but no param
                song = answer
                song_dict = get_songlist(song)
                if len(song_dict) > 0:                                          # list not empty
                    answer = show_list(song_dict)
                else:
                    print('Warning: nothing found')
                    answer = input(question)
            else:
                print('Warning: invalid input')
                finish = True
        elif answer == 'q':
            finish = True
        elif answer == 's' or answer == '-s' or answer == '--songs':            # show list of downloaded songs
            show_downloaded_songs()
            answer = input(question)
        elif answer == 'l':
            if len(song_dict) > 0:                                              # list not empty
                answer = show_list(song_dict)
            else:
                print('Warning: There is no search result')
                answer = input(question)
        elif answer.isnumeric():                                                # song number selected
            if len(song_dict) == 0:                                             # nothing found
                print('Warning: Youtube found nothing')
                answer = input(question)
            else:
                song = song_dict[answer][0]                                     # get song name
                audio_path = get_song(song)                                     # download song
                play_song(audio_path)                                           # play song
                answer = input(question)
        elif answer[0] != '-' and answer[:2] != '--':                           # expect song title from cli
            song = answer
            song_dict = get_songlist(song)
            if len(song_dict) > 0:                                              # list not empty
                answer = show_list(song_dict)
            else:
                print('Warning: nothing found')
                answer = input(question)
        else:
            print('Error: invalid input')
            finish = True
            
    print(THANKS)
    return 0


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
